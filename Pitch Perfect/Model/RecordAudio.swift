//
//  RecordAudio.swift
//  Pitch Perfect
//
//  Created by Ali Rezaei on 15/10/15.
//  Copyright © 2015 Dynamo. All rights reserved.
//

import Foundation

class RecordedAudio: NSObject {
    
    var filePathUrl: NSURL!
    var title: String!
    
}
